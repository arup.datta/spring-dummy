package com.example.springdummy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringDummyApplication {

	@GetMapping("/message")
	public String dummyFunc() {
		return "Random text from dummy app";
	}

	@GetMapping("/")
	public String dummyHome() {
		return "Dummy home loaded";
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringDummyApplication.class, args);

		System.out.println("Hello from my dummy app");
	}

}
