FROM openjdk:8
EXPOSE 8081
ADD target/spring-dummy.jar spring-dummy.jar
ENTRYPOINT ["java","-jar","spring-dummy.jar"]